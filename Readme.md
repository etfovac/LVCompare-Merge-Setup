You can now use Labview's built-in tools to compare/diff and merge from git:

# Setup LVTools:

Copy this code and run it in Git Bash 

`cd && git clone https://gitlab.com/sas-blog/LVCompare-Merge-Setup.git && cd LVCompare-Merge-Setup && ./setupLVTools.sh`

This will clone the repo and then copy appropriate scripts to `~/bin` on your local drive, and set a new default for the mergetool and difftool commands. 

# Using diff/comparing changes

* To compare revisions, file-by-file use `git difftool`. It has the same syntax as `git diff`. Typing `git help diff` or `git help difftool` should supply all the information you need.
* To compare a specific VI only add `-- path/to/file.vi` to the end of your `git difftool` command.

# Resolving merge conflicts

The `git mergetool` command has similar syntax to the `git merge` command. Typing `git help merge` or `git help mergetool` should supply all the information you need.


# Credit

This code was shamelessly copied from [here](https://github.com/JQIamo/SetList) and then the setup script modified to make it apply globally instead of just to the local repository and to set it as the default. The wrapper scripts were basically untouched.
